import pygame
from pygame import *
from human import *
from blocks import *


WIN_WIDHT = 800
WIN_HEIGHT = 600
DISPLAY = (WIN_WIDHT, WIN_HEIGHT)
BACKGROUND_COLOR = "#004400"
PLATFORM_WIDHT = 32
PLATFORM_HEIGHT = 32
PLATFORM_COLOR = "#990000"


def main():
    hero = Player(55, 55)
    left = right = up = 0
    entities = sprite.Group()
    platforms = []
    entities.add(hero)
    level = [
        "-------------------------",
        "-                       -",
        "-                       -",
        "-            --         -",
        "-                       -",
        "--                      -",
        "-                       -",
        "-                   --- -",
        "-                       -",
        "-                       -",
        "-      ---              -",
        "-                       -",
        "-   -----------         -",
        "-                       -",
        "-                -      -",
        "-                   --  -",
        "-                       -",
        "-                       -",
        "-------------------------"]
    timer = pygame.time.Clock()

    pygame.init()
    screen = pygame.display.set_mode(DISPLAY)
    pygame.display.set_caption("The binding of super Meatboy afterbirth plus ")
    bg = Surface((WIN_WIDHT, WIN_HEIGHT))
    bg.fill(Color(BACKGROUND_COLOR))
    while True:
        timer.tick(60)
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                raise(SystemExit, "QUIT")
            if e.type == pygame.KEYDOWN and e.key == pygame.K_LEFT:
                left = True
            if e.type == pygame.KEYDOWN and e.key == pygame.K_RIGHT:
                right = True
            if e.type == pygame.KEYDOWN and e.key == pygame.K_UP:
                up = True
            if e.type == pygame.KEYUP and e.key == pygame.K_UP:
                up = False

            if e.type == pygame.KEYUP and e.key == pygame.K_LEFT:
                left = False
            if e.type == pygame.KEYUP and e.key == pygame.K_RIGHT:
                right = False
        screen.blit(bg, (0, 0))

        x = y =0
        for row in level:
            for col in row:
                if col == "-":
                    pf = Platform(x, y)
                    entities.add(pf)
                    platforms.append(pf)
                x+= PLATFORM_WIDHT
            y+= PLATFORM_HEIGHT
            x=0
        hero.update(left, right, up, platforms)
        entities.draw(screen)
        pygame.display.update()
if __name__ == "__main__":
    main()
