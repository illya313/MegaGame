import pygame
from pygame import *


MOVE_SPEED = 7
WIDHT = 22
HEIGTH = 32
COLOR = "#000099"
JUMP_POWER = 10
GRAVITY = 0.35

class Player(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.xvel = 0
        self.startX = x
        self.image = image.load("player.png")
        self.rect = pygame.Rect(x, y, WIDHT, HEIGTH)
        self.yvel = 0
        self.onGround = False

    def update(self, left, right, up, platforms):
        if left:
            self.xvel = -MOVE_SPEED
        if up:
            if self.onGround:
                self.yvel = -JUMP_POWER
        if right:
            self.xvel = MOVE_SPEED
        if not(right or left):
            self.xvel = 0
        if not(self.onGround):
            self.yvel += GRAVITY
        self.onGround = False
        self.rect.y += self.yvel
        self.collide(0, self.yvel, platforms)
        self.rect.x += self.xvel
        self.collide(self.xvel, 0, platforms)

    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))

    def collide(self, xvel, yvel, platforms):
        for p in platforms:
            if sprite.collide_rect(self, p):
                if xvel > 0:
                    self.rect.right = p.rect.left
                if xvel < 0:
                    self.rect.left =  p.rect.right
                if yvel > 0:
                    self.rect.bottom = p.rect.top
                    self.onGround = True
                    self.yvel = 0
                if yvel < 0 :
                    self.rect.top = p.rect.bottom
                    self.yvel = 0